package de.myzelyam.lotcmiscellaneous.chunk;

import java.util.LinkedList;
import java.util.List;

import org.bukkit.*;
import org.bukkit.configuration.ConfigurationSection;

import de.myzelyam.lotcmiscellaneous.AbstractModule;
import de.myzelyam.lotcmiscellaneous.LOTCMiscellaneous;

public class ChunkLoaderModule extends AbstractModule {

    public ChunkLoaderModule(LOTCMiscellaneous instance) {
        super(instance);
    }

    @Override
    public boolean enable() {
        new EventListener(this);
        return true;
    }

    @Override
    public void disable(LOTCMiscellaneous plugin) {
    }

    public List<Chunk> getForceLoadedChunks() {
        ConfigurationSection section = this.getPlugin().getConfig()
                .getConfigurationSection("LoadedChunks");
        List<Chunk> chunks = new LinkedList<>();
        for (String worldStr : section.getKeys(false)) {
            List<String> chunksXZ = this.getPlugin().getConfig()
                    .getStringList("LoadedChunks." + worldStr);
            World world = Bukkit.getWorld(worldStr);
            if (world == null) {
                continue;
            }
            for (String chunkXZ : chunksXZ) {
                int x, z;
                String[] splitted = chunkXZ.split(":");
                x = Integer.parseInt(splitted[0]);
                z = Integer.parseInt(splitted[1]);
                Chunk chunk = world.getChunkAt(x, z);
                chunks.add(chunk);
            }
        }
        return chunks;
    }
}

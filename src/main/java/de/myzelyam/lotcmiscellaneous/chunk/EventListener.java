package de.myzelyam.lotcmiscellaneous.chunk;

import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.world.ChunkUnloadEvent;

public class EventListener implements Listener {

	private ChunkLoaderModule module;

	public EventListener(ChunkLoaderModule module) {
		this.module = module;
		Bukkit.getPluginManager().registerEvents(this, module.getPlugin());
	}

	@EventHandler
	public void onChunkUnload(ChunkUnloadEvent e) {
		Chunk c = e.getChunk();
		if (module.getForceLoadedChunks().contains(c))
			e.setCancelled(true);
	}
}

package de.myzelyam.lotcmiscellaneous;

public abstract class AbstractModule {

        private LOTCMiscellaneous plugin;
    
        public AbstractModule(LOTCMiscellaneous instance) {
            this.plugin = instance;
        }
    
	/**
	 * Enables a module
	 * 
	 * @return If the module has been enabled successfully
	 */
	public abstract boolean enable();

	/**
	 * Disables a module
	 * 
	 * @param plugin
	 *            The plugin instance of LOTCMiscellaneous
	 */
	public abstract void disable(LOTCMiscellaneous plugin);
        
        public LOTCMiscellaneous getPlugin() {
            return this.plugin;
        }
}

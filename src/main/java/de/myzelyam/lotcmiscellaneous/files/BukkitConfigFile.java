package de.myzelyam.lotcmiscellaneous.files;

import java.io.File;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import de.myzelyam.lotcmiscellaneous.LOTCMiscellaneous;

public class BukkitConfigFile implements PluginFile<FileConfiguration> {

	public BukkitConfigFile(String name) {
		this.name = (name + ".yml");
		this.plugin = LOTCMiscellaneous.getInstance();
		setup();
	}

	private LOTCMiscellaneous plugin;

	private String name;

	private File file;

	private FileConfiguration fileConfiguration;

	@Override
	public String getName() {
		return name;
	}

	private void setup() {
		this.file = new File(plugin.getDataFolder(), name);
		save();
	}

	@Override
	public void reload() {
		fileConfiguration = YamlConfiguration.loadConfiguration(file);
	}

	@Override
	public FileConfiguration getConfig() {
		if (fileConfiguration == null) {
			this.reload();
		}
		return fileConfiguration;
	}

	@Override
	public void save() {
		if (!file.exists()) {
			plugin.saveResource(name, false);
		}
	}
}

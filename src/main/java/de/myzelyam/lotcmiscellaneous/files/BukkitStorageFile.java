package de.myzelyam.lotcmiscellaneous.files;

import java.io.File;
import java.io.IOException;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import de.myzelyam.lotcmiscellaneous.LOTCMiscellaneous;

public class BukkitStorageFile implements PluginFile<FileConfiguration> {

	public BukkitStorageFile(String name) {
		this.name = name;
		this.plugin = LOTCMiscellaneous.getInstance();
		setup();
	}

	private String name;

	private File file;

	private FileConfiguration config;

	private LOTCMiscellaneous plugin;

	@Override
	public String getName() {
		return name;
	}

	private void setup() {
		file = new File(plugin.getDataFolder().getPath() + File.separator + name
				+ ".yml");
		config = YamlConfiguration.loadConfiguration(file);
		save();
	}

	@Override
	public void reload() {
		setup();
	}

	@Override
	public FileConfiguration getConfig() {
		return config;
	}

	@Override
	public void save() {
		try {
			config.save(file);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}

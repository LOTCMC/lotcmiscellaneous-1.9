package de.myzelyam.lotcmiscellaneous.files;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import de.myzelyam.lotcmiscellaneous.bungee.LOTCBungee;

import net.md_5.bungee.config.*;

public class BungeeConfigFile implements PluginFile<Configuration> {

	public BungeeConfigFile(String name) {
		this.name = (name + ".yml");
		setup();
	}

	private LOTCBungee plugin = LOTCBungee.getInstance();

	private String name;

	private File file;

	private Configuration fileConfiguration;

	@Override
	public String getName() {
		return name;
	}

	private void setup() {
		file = new File(plugin.getDataFolder(), name);
		createFileIfRequired();
		try {
			fileConfiguration = ConfigurationProvider
					.getProvider(YamlConfiguration.class).load(file);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void reload() {
		setup();
	}

	@Override
	public Configuration getConfig() {
		if (fileConfiguration == null) {
			this.reload();
		}
		return fileConfiguration;
	}

	@Override
	public void save() {
		createFileIfRequired();
	}

	private void createFileIfRequired() {
		if (!plugin.getDataFolder().exists())
			plugin.getDataFolder().mkdir();
		File file = new File(plugin.getDataFolder(), name);
		if (!file.exists()) {
			try {
				Files.copy(plugin.getResourceAsStream(name), file.toPath());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}

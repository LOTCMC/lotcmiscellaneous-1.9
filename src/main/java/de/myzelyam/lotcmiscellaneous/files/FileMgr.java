package de.myzelyam.lotcmiscellaneous.files;

import java.util.HashMap;
import java.util.Map;

public class FileMgr {

	public static FileMgr instance;

	public FileMgr() {
		instance = this;
		files = new HashMap<>();
	}

	private Map<String, PluginFile<?>> files;

	public PluginFile<?> addFile(String name, FileType type) {
		if (name == null)
			throw new IllegalArgumentException("The file name cannot be null!");
		if (type == FileType.BUKKIT_STORAGE) {
			PluginFile<?> file = new BukkitStorageFile(name);
			files.put(name, file);
			return file;
		} else if (type == FileType.BUKKIT_CONFIG) {
			PluginFile<?> file = new BukkitConfigFile(name);
			files.put(name, file);
			return file;
		} else if (type == FileType.BUNGEE_CONFIG) {
			PluginFile<?> file = new BungeeConfigFile(name);
			files.put(name, file);
			return file;
		} else {
			throw new IllegalArgumentException("The FileType cannot be null!");
		}
	}

	public static PluginFile<?> getFile(String name) {
		PluginFile<?> file = FileMgr.instance.files.get(name);
		if (file == null)
			return null;
		return file;
	}

	public enum FileType {
		BUKKIT_STORAGE, BUKKIT_CONFIG, BUNGEE_CONFIG;
	}

	public void reloadFile(String fileName) {
		PluginFile<?> file = files.get(fileName);
		if (file != null)
			file.reload();
		else
			throw new IllegalArgumentException(
					"[LOTCMiscellaneous] Specified file doesn't exist!");
	}

	public void reloadAll() {
		for (String fileName : files.keySet())
			reloadFile(fileName);
	}
}

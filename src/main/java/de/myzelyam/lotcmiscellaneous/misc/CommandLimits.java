package de.myzelyam.lotcmiscellaneous.misc;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

import de.myzelyam.lotcmiscellaneous.AbstractModule;
import de.myzelyam.lotcmiscellaneous.LOTCMiscellaneous;
import org.bukkit.Bukkit;

public class CommandLimits extends AbstractModule implements Listener {

    public CommandLimits(LOTCMiscellaneous instance) {
        super(instance);
    }

    @EventHandler
    public void onCmd(PlayerCommandPreprocessEvent e) {
        Player p = e.getPlayer();
        // anti pex exploit
        try {
            if (p.hasPermission("lotc.pex")) {
                return;
            }
            String label = e.getMessage().toLowerCase().substring(1,
                    e.getMessage().toLowerCase().length());
            if (label.contains(" ")) {
                label = label.split(" ")[0];
            }
            if (label.contains(":") && !label.endsWith(":")) {
                label = label.split(":")[1];
            }
            if (label.equals("pex") || label.equals("promote")
                    || label.equals("demote")) {
                e.setCancelled(true);
                p.sendMessage("Unknown command. Do \"/modreq\" for help.");
                return;
            }
        } catch (Exception e2) {
        }
    }

    @Override
    public boolean enable() {
        Bukkit.getServer().getPluginManager().registerEvents(this, this.getPlugin());
        return true;
    }

    @Override
    public void disable(LOTCMiscellaneous plugin) {
    }
}

package de.myzelyam.lotcmiscellaneous.commands;

import de.myzelyam.lotcmiscellaneous.LOTCMiscellaneous;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.entity.Player.Spigot;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.*;
import net.md_5.bungee.api.chat.ComponentBuilder.FormatRetention;

public class HelpModule extends AbstractCommand {

    public HelpModule(LOTCMiscellaneous instance) {
        super(instance, "help");
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command,
            String label, String[] args) {
        if (!(sender instanceof Player)) {
            sender.sendMessage("§cYou must be a player!");
            return true;
        }
        Spigot p = ((Player) sender).spigot();
        ClickEvent wikiCE = new ClickEvent(ClickEvent.Action.OPEN_URL,
                "http://www.landofthecrafters.com/wiki/minecraft-index/");
        HoverEvent wikiHE = new HoverEvent(HoverEvent.Action.SHOW_TEXT,
                new ComponentBuilder("Click here to visit our wiki!")
                .color(ChatColor.GOLD).create());
        ClickEvent modreqCE = new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND,
                "/modreq ");
        HoverEvent modreqHE = new HoverEvent(HoverEvent.Action.SHOW_TEXT,
                new ComponentBuilder("Click here to do /modreq!")
                .color(ChatColor.GOLD).create());
        ComponentBuilder c = new ComponentBuilder("Please visit our ")
                .color(ChatColor.YELLOW).append("wiki").event(wikiCE)
                .event(wikiHE).color(ChatColor.AQUA).underlined(true)
                .append(" or use ", FormatRetention.NONE)
                .color(ChatColor.YELLOW).append("/modreq").event(modreqCE)
                .event(modreqHE).color(ChatColor.AQUA).underlined(true)
                .append(" to request help from our moderators.",
                        FormatRetention.NONE)
                .color(ChatColor.YELLOW);
        p.sendMessage(c.create());
        return true;
    }
}

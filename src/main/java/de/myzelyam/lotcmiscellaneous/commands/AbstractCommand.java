package de.myzelyam.lotcmiscellaneous.commands;

import org.bukkit.command.CommandExecutor;
import org.bukkit.command.PluginCommand;

import de.myzelyam.lotcmiscellaneous.AbstractModule;
import de.myzelyam.lotcmiscellaneous.LOTCMiscellaneous;

public abstract class AbstractCommand extends AbstractModule
        implements CommandExecutor {

    private String commandName;

    /**
     *
     * @param instance
     * @param command
     */
    public AbstractCommand(LOTCMiscellaneous instance, String command) {
        super(instance);
        this.commandName = command;
    }

    @Override
    public boolean enable() {
        // register command
        PluginCommand cmd = this.getPlugin().getCommand(commandName);
        if (cmd == null) {
            return false;
        }
        cmd.setExecutor(this);
        return true;
    }

    @Override
    public void disable(LOTCMiscellaneous plugin) {
        // not required, command can override
    }
}

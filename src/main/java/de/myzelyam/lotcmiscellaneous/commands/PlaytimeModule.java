package de.myzelyam.lotcmiscellaneous.commands;

import de.myzelyam.lotcmiscellaneous.LOTCMiscellaneous;
import de.myzelyam.lotcmiscellaneous.hooks.VanishHook;
import de.myzelyam.lotcmiscellaneous.utils.MathUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.Statistic;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.Date;
import java.util.concurrent.TimeUnit;

public class PlaytimeModule extends AbstractCommand {

    public PlaytimeModule(LOTCMiscellaneous instance) {
        super(instance, "playtime");
    }

    @Override
    public boolean onCommand(final CommandSender sender, Command command,
            String label, final String[] args) {
        if (!sender.hasPermission("lotc.cmd.playtime")) {
            sender.sendMessage(ChatColor.DARK_RED
                    + "Denied access! You are not allowed to do this.");
            return true;
        }
        if (args.length < 1 && !(sender instanceof Player)) {
            return false;
        }
        new BukkitRunnable() {

            @SuppressWarnings("deprecation")
            @Override
            public void run() {
                OfflinePlayer target;
                if (args.length < 1) {
                    target = (OfflinePlayer) sender;
                } else {
                    target = Bukkit.getPlayer(args[0]);
                    if (target == null) {
                        target = fetchOP(args[0]);
                    }
                }
                boolean hidden = sender instanceof Player && target instanceof Player
                        && (Bukkit.getPluginManager().isPluginEnabled("SuperVanish")
                        || Bukkit.getPluginManager().isPluginEnabled("PremiumVanish"))
                        && !VanishHook.canSee((Player) sender, (Player) target);
                if (target == null) {
                    sender.sendMessage(ChatColor.RED
                            + "This player has never joined this server before.");
                } else {
                    Player online = target.getPlayer();
                    sender.sendMessage(ChatColor.YELLOW
                            + "<< " + ChatColor.GOLD + "Stats for " + target.getName() + ChatColor.YELLOW + " >>");
                    Date firstJoined = new Date(target.getFirstPlayed());
                    sender.sendMessage(
                            ChatColor.RED + "First joined: " + ChatColor.GREEN + firstJoined.getDate() + "."
                            + firstJoined.getMonth() + "."
                            + (firstJoined.getYear() + 1900));
                    if (online != null && !hidden) {
                        int playedHours = online
                                .getStatistic(Statistic.PLAY_ONE_TICK)
                                / (20 /* now seconds */ * 60 /* now minutes */
                                * 60 /* now hours */);
                        long hoursSinceFirstJoin = TimeUnit.MILLISECONDS
                                .toHours(new Date().getTime()
                                        - target.getFirstPlayed());
                        sender.sendMessage(ChatColor.RED + "Playtime: " + ChatColor.YELLOW + playedHours
                                + "h" + ChatColor.GREEN + "=" + ChatColor.YELLOW + playedHours / 24 + "d"
                                + ChatColor.GREEN + " over " + ChatColor.YELLOW
                                + +(hoursSinceFirstJoin / 24)
                                + "d" + ChatColor.GREEN + " since first join (" + ChatColor.YELLOW
                                + +MathUtils
                                .roundToTwoDecimals(((float) playedHours
                                        / (float) hoursSinceFirstJoin)
                                        * 100f)
                                + "%" + ChatColor.GREEN + " online)");
                    } else {
                        sender.sendMessage(ChatColor.RED + "Playtime: " + ChatColor.GREEN + "Player not online");
                    }
                }
            }
        }.runTaskAsynchronously(this.getPlugin());
        return true;
    }

    public OfflinePlayer fetchOP(String name) {
        for (OfflinePlayer op : Bukkit.getOfflinePlayers()) {
            if (op.getName().equalsIgnoreCase(name)) {
                return op;
            }
        }
        return null;
    }
}

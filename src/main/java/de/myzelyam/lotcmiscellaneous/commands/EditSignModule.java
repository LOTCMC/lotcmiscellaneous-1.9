package de.myzelyam.lotcmiscellaneous.commands;

import java.util.Set;

import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import de.myzelyam.lotcmiscellaneous.LOTCMiscellaneous;

public class EditSignModule extends AbstractCommand {

	public EditSignModule(LOTCMiscellaneous instance) {
		super(instance, "editsign");
	}

	@Override
	public boolean onCommand(CommandSender sender,
			org.bukkit.command.Command command, String label, String[] args) {
		if (!(sender instanceof Player)) {
			sender.sendMessage("§cYou must be a player!");
			return true;
		}
		Player p = (Player) sender;
		try {
			if (!p.hasPermission("lotc.cmd.editsign")) {
				p.sendMessage(
						"§4Denied access! You are not allowed to do this.");
				return true;
			}
			if (args.length < 2) {
				p.sendMessage("§cInvalid usage! /editsign <line> <value>");
				return true;
			}
			Block block = p.getTargetBlock((Set<Material>) null, 10);
			if (!(block != null && block.getState() instanceof Sign)) {
				p.sendMessage("§cYour targeted block is no sign!");
				return true;
			}
			Sign s = (Sign) block.getState();
			int line = Integer.parseInt(args[0]);
			if (line < 1 || line > 4)
				throw new NumberFormatException();
			int sline = line - 1;
			String newline = "";
			for (int i = 1; i < args.length; i++) {
				newline = newline.concat(args[i]);
				newline = newline.concat(" ");
			}
			// check (not 100%!!)
			if (p.getGameMode() == GameMode.ADVENTURE) {
				p.sendMessage(
						"§cPermission violation: You can't edit in this state.");
				return true;
			}
			WorldGuardPlugin wg = (WorldGuardPlugin) Bukkit.getServer()
					.getPluginManager().getPlugin("WorldGuard");
			if (!wg.canBuild(p, block)) {
				p.sendMessage(
						"§cPermission violation: You can't edit this block.");
				return true;
			}
			if (!this.getPlugin().getConfig().getStringList("EditSign.WhitelistedWorlds")
					.contains(p.getWorld().getName().toLowerCase())
					&& !p.hasPermission("lotc.bypass.editsign")) {
				p.sendMessage("§cPermission violation: Invalid world.");
				return true;
			}
			// update
			s.setLine(sline,
					ChatColor.translateAlternateColorCodes('&', newline)
							.replace("''", ""));
			s.update();
			p.sendMessage("§aSuccessfully changed line §e" + line
					+ " §aof the sign!");
		} catch (NumberFormatException e) {
			p.sendMessage("§c\"§e" + args[0]
					+ "§c\" is not a valid number!\n§eAllowed numbers: 1§c,§e 2§c,§e 3§c,§e 4");
			return true;
		}
		return true;
	}
}

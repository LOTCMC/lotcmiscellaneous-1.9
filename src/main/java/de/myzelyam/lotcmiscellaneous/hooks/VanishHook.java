package de.myzelyam.lotcmiscellaneous.hooks;

import de.myzelyam.api.vanish.VanishAPI;
import org.bukkit.entity.Player;

/* You still have to check if SuperVanish or PremiumVanish is actually enabled first */
public abstract class VanishHook {

    public static boolean canSee(Player p1, Player p2) {
        return VanishAPI.canSee(p1, p2);
    }
}

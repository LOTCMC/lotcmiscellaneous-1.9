package de.myzelyam.lotcmiscellaneous;

import net.bnetdev.lotcmiscellaneous.world.BlockModule;
import de.myzelyam.lotcmiscellaneous.chunk.ChunkLoaderModule;
import de.myzelyam.lotcmiscellaneous.commands.EditSignModule;
import de.myzelyam.lotcmiscellaneous.commands.HelpModule;
import de.myzelyam.lotcmiscellaneous.commands.PlaytimeModule;
import de.myzelyam.lotcmiscellaneous.files.FileMgr;
import de.myzelyam.lotcmiscellaneous.files.FileMgr.FileType;
import de.myzelyam.lotcmiscellaneous.files.PluginFile;
import de.myzelyam.lotcmiscellaneous.misc.CommandLimits;
import de.myzelyam.lotcmiscellaneous.music.MusicModule;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import net.bnetdev.lotcmiscellaneous.world.FlyModule;

public class LOTCMiscellaneous extends JavaPlugin {

    private static LOTCMiscellaneous instance;
    private LOTCMiscellaneous plugin;

    public AbstractModule[] REGISTERED_MODULES;

    public static final String PLUGIN_PREFIX = "[LOTCMiscellaneous] ";

    public static final boolean DEBUG = false;

    private List<AbstractModule> loadedModules;

    public PluginFile<FileConfiguration> dataFile;

    public FileConfiguration data;

    private FileMgr fileMgr;

    public static LOTCMiscellaneous getInstance() {
        return instance;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void onEnable() {
        this.plugin = this;
        this.initModules();
        instance = this;
        saveDefaultConfig();
        fileMgr = new FileMgr();
        dataFile = (PluginFile<FileConfiguration>) fileMgr.addFile("data",
                FileType.BUKKIT_STORAGE);
        data = dataFile.getConfig();
        loadedModules = new LinkedList<>();
        log("Enabling modules...");
        for (AbstractModule module : this.REGISTERED_MODULES) {
            boolean successful = module.enable();
            if (successful) {
                loadedModules.add(module);
            }
            String name = module.getClass().getSimpleName();
            if (successful) {
                log("Successfully enabled " + name + "!");
            } else {
                log(Level.SEVERE, "Failed to enable " + name + "!");
            }
        }
    }

    @Override
    public void onDisable() {
        log("Disabling modules...");
        for (AbstractModule module : loadedModules) {
            module.disable(this);
        }
        loadedModules.clear();
        instance = null;
    }

    /**
     * Logs a message using Bukkit's logger
     *
     * @param level The level to be used for logging
     * @param msg The message to be sent WITHOUT prefix
     */
    public static void log(Level level, String msg) {
        Bukkit.getLogger().log(level, PLUGIN_PREFIX + msg);
    }

    /**
     * Logs a message using Bukkit's logger with INFO level
     *
     * @param msg The message to be sent WITHOUT prefix
     */
    public static void log(String msg) {
        Bukkit.getLogger().log(Level.INFO, PLUGIN_PREFIX + msg);
    }

    public static void debug(String string) {
        if (DEBUG) {
            Bukkit.getLogger().log(Level.INFO,
                    PLUGIN_PREFIX + "DEBUG: " + string);
        }
    }

    private void initModules() {
        REGISTERED_MODULES = new AbstractModule[]{
            new EditSignModule(this.plugin), new MusicModule(this.plugin), new PlaytimeModule(this.plugin),
            new CommandLimits(this.plugin), new ChunkLoaderModule(this.plugin), new HelpModule(this.plugin),
            new BlockModule(this.plugin), new FlyModule(this.plugin)};
    }
}

package de.myzelyam.lotcmiscellaneous.music;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;
import java.util.logging.Level;

import javax.annotation.CheckForNull;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import de.myzelyam.lotcmiscellaneous.AbstractModule;
import de.myzelyam.lotcmiscellaneous.LOTCMiscellaneous;
import de.myzelyam.lotcmiscellaneous.utils.OneDotEightFeatures;
import de.myzelyam.lotcmiscellaneous.utils.StringUtils;

public class MusicModule extends AbstractModule {

    private SongPlayer songPlayer;

    private List<NoteSong> noteSongs;

    public MusicModule(LOTCMiscellaneous instance) {
        super(instance);
    }

    @Override
    public boolean enable() {
        songPlayer = new SongPlayer(this);
        // initsongs
        noteSongs = initNoteSongs();
        // player listener
        new PlayerListener(this);
        // cmds
        getPlugin().getCommand("music")
                .setExecutor(new MusicCommand(getPlugin(), this));
        // reload check
        for (Player p : Bukkit.getOnlinePlayers()) {
            Location loc = p.getLocation();
            if (enabled(p)) {
                if (StringUtils.containsIgnoreCase(getMusicWorldNames(),
                        loc.getWorld().getName())) {
                    // play music
                    getSongPlayer().playSong(p,
                            getRandomNoteSong(p.getWorld().getName()));
                    sendSongMsg(p);
                }
            }
        }
        return true;
    }

    @Override
    public void disable(LOTCMiscellaneous plugin) {
        songPlayer.terminateEverything();
    }

    public SongPlayer getSongPlayer() {
        return songPlayer;
    }

    public NoteSong getRandomNoteSong() {
        int random = new Random().nextInt(noteSongs.size());
        return noteSongs.get(random);
    }

    @CheckForNull
    public NoteSong getRandomNoteSong(String world) {
        LOTCMiscellaneous.debug("start random for world " + world);
        String playlist = null;
        try {
            for (String str : this.getPlugin().getConfig().getStringList("MusicWorlds")) {
                if (str.split(">")[0].equalsIgnoreCase(world)) {
                    playlist = str.split(">")[1];
                }
            }
        } catch (Exception e) {
            playlist = null;
            LOTCMiscellaneous.log(Level.WARNING,
                    "Music: Invalid config setting: \"MusicWorlds\" or \"Playlist\"");
        }
        LOTCMiscellaneous.debug("playlist => " + playlist);
        List<NoteSong> matchingSongs = new ArrayList<>(this.noteSongs);
        List<String> playlistSongs = this.getPlugin().getConfig()
                .getStringList("Playlist." + playlist);
        if (playlist.equalsIgnoreCase("all")) {
            playlistSongs = new LinkedList<>(getTitleSongMap().keySet());
        }
        LOTCMiscellaneous.debug("playlist songs #1 => " + playlistSongs);
        if (playlist != null) {
            allLoop:
            for (NoteSong song : new ArrayList<>(matchingSongs)) {
                for (String title : playlistSongs) {
                    if (song.getTitle().equalsIgnoreCase(title)) {
                        continue allLoop;
                    }
                }
                LOTCMiscellaneous.debug("removing " + song.getTitle());
                matchingSongs.remove(song);
            }
        }
        LOTCMiscellaneous.debug("playlist songs #2 => " + playlistSongs);
        if (matchingSongs.size() < 1) {
            return null;
        }
        int random = new Random().nextInt(matchingSongs.size());
        LOTCMiscellaneous.debug("end random for world " + world + "; result: "
                + matchingSongs.get(random));
        return matchingSongs.get(random);
    }

    public List<NoteSong> getAllNoteSongs() {
        return noteSongs;
    }

    public List<NoteSong> initNoteSongs() {
        List<NoteSong> songs = new LinkedList<>();
        File folder = getPlugin().getDataFolder();
        for (File file : folder.listFiles()) {
            if (file.getName().endsWith(".nbs")) {
                try {
                    songs.add(NBSDecoder.parse(file));
                } catch (FileNotFoundException e) {
                }
            }
        }
        return songs;
    }

    public void sendSongMsg(final Player p) {
        final String title = getSongPlayer().getPlayingSong(p).getTitle();
        OneDotEightFeatures.sendActionBar(p, "§6Now playing: §c" + title);
        new BukkitRunnable() {

            @Override
            public void run() {
                OneDotEightFeatures.sendActionBar(p,
                        "§6Now playing: §c" + title);
            }
        }.runTaskLaterAsynchronously(getPlugin(), 30);
    }

    private boolean enabled(Player p) {
        boolean enabled = getPlugin().data.getBoolean("Music.PlayerData."
                + p.getUniqueId().toString() + ".musicEnabled", true);
        return enabled;
    }

    @CheckForNull
    public NoteSong getSongByString(String str) {
        Map<String, NoteSong> map = getTitleSongMap();
        // equals ignore case?
        for (String titleKey : map.keySet()) {
            if (titleKey.equalsIgnoreCase(str)
                    || titleKey.trim().equalsIgnoreCase(str)) {
                return map.get(titleKey);
            }
        }
        // starts with ignore case?
        for (String titleKey : map.keySet()) {
            if (titleKey.toUpperCase().startsWith(str.toUpperCase()) || titleKey
                    .trim().toUpperCase().equalsIgnoreCase(str.toUpperCase())) {
                return map.get(titleKey);
            }
        }
        // highest similarity > 0.7?
        NoteSong currentSong = null;
        double currentSimilarity = 0.0;
        for (String titleKey : map.keySet()) {
            double similarity = StringUtils.similarity(titleKey.toUpperCase(),
                    str.toUpperCase());
            if (similarity > currentSimilarity) {
                currentSimilarity = similarity;
                currentSong = map.get(titleKey);
            }
        }
        return ((currentSimilarity > 0.7) ? currentSong : null);
    }

    public Map<String, NoteSong> getTitleSongMap() {
        Map<String, NoteSong> titleSongMap = new HashMap<>();
        for (NoteSong song : getAllNoteSongs()) {
            titleSongMap.put(song.getTitle(), song);
        }
        return titleSongMap;
    }

    public List<String> getMusicWorldNames() {
        List<String> musicWorlds = new LinkedList<>();
        for (String str : getPlugin().getConfig()
                .getStringList("MusicWorlds")) {
            musicWorlds.add(str.split(">")[0]);
        }
        return musicWorlds;
    }
}

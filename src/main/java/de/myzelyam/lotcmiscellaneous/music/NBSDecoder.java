package de.myzelyam.lotcmiscellaneous.music;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

/**
 * @author xxmicloxx, michidk, MyzelYam
 */
public class NBSDecoder {

	public static NoteSong parse(File decodeFile) throws FileNotFoundException {
		return parse(new FileInputStream(decodeFile), decodeFile);
	}

	public static NoteSong parse(InputStream inputStream) {
		return parse(inputStream, null); // Source is unknown -> no file
	}

	private static NoteSong parse(InputStream inputStream, File decodeFile) {
		Map<Integer, SongLayer> layerMap = new HashMap<Integer, SongLayer>();
		try {
			DataInputStream dataInputStream = new DataInputStream(inputStream);
			short length = readShort(dataInputStream);
			short noteSongHeight = readShort(dataInputStream);
			String title = readString(dataInputStream);
			String author = readString(dataInputStream);
			readString(dataInputStream);
			String description = readString(dataInputStream);
			float speed = readShort(dataInputStream) / 100f;
			dataInputStream.readBoolean(); // auto-save
			dataInputStream.readByte(); // auto-save duration
			dataInputStream.readByte(); // x/4ths, time signature
			readInt(dataInputStream); // minutes spent on project
			readInt(dataInputStream); // left clicks (why?)
			readInt(dataInputStream); // right clicks (why?)
			readInt(dataInputStream); // blocks added
			readInt(dataInputStream); // blocks removed
			readString(dataInputStream); // .mid/.schematic file name
			short tick = -1;
			while (true) {
				short jumpTicks = readShort(dataInputStream); // jumps till next
																// tick
				// System.out.println("Jumps to next tick: " + jumpTicks);
				if (jumpTicks == 0) {
					break;
				}
				tick += jumpTicks;
				// System.out.println("Tick: " + tick);
				short layer = -1;
				while (true) {
					short jumpLayers = readShort(dataInputStream); // jumps till
																	// next
																	// layer
					if (jumpLayers == 0) {
						break;
					}
					layer += jumpLayers;
					// System.out.println("Layer: " + layer);
					setNote(layer, tick,
							dataInputStream.readByte() /* instrument */,
							dataInputStream.readByte() /* note */, layerMap);
				}
			}
			for (int i = 0; i < noteSongHeight; i++) {
				SongLayer layer = layerMap.get(i);
				if (layer != null) {
					layer.setName(readString(dataInputStream));
					layer.setVolume(dataInputStream.readByte());
				}
			}
			return new NoteSong(speed, layerMap, noteSongHeight, length, title,
					author, description, decodeFile);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	private static void setNote(int layerId, int ticks, byte instrument,
			byte key, Map<Integer, SongLayer> layerMap) {
		SongLayer layer = layerMap.get(layerId);
		if (layer == null) {
			layer = new SongLayer();
			layerMap.put(layerId, layer);
		}
		layer.setNote(ticks, new Note(instrument, key));
	}

	private static short readShort(DataInputStream dis) throws IOException {
		int byte1 = dis.readUnsignedByte();
		int byte2 = dis.readUnsignedByte();
		return (short) (byte1 + (byte2 << 8));
	}

	private static int readInt(DataInputStream dis) throws IOException {
		int byte1 = dis.readUnsignedByte();
		int byte2 = dis.readUnsignedByte();
		int byte3 = dis.readUnsignedByte();
		int byte4 = dis.readUnsignedByte();
		return (byte1 + (byte2 << 8) + (byte3 << 16) + (byte4 << 24));
	}

	private static String readString(DataInputStream dis) throws IOException {
		int length = readInt(dis);
		StringBuilder sb = new StringBuilder(length);
		for (; length > 0; --length) {
			char c = (char) dis.readByte();
			if (c == (char) 0x0D) {
				c = ' ';
			}
			sb.append(c);
		}
		return sb.toString();
	}
}

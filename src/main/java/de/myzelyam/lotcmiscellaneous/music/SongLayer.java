package de.myzelyam.lotcmiscellaneous.music;

import java.util.HashMap;
import java.util.Map;

/**
 * @author xxmicloxx, michidk, MyzelYam
 */
public class SongLayer {

	private Map<Integer, Note> noteMap = new HashMap<Integer, Note>();

	private byte volume = 100;

	private String name = "";

	public Map<Integer, Note> getMap() {
		return noteMap;
	}

	public void setMap(Map<Integer, Note> map) {
		this.noteMap = map;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Note getNote(int tick) {
		return noteMap.get(tick);
	}

	public void setNote(int tick, Note note) {
		noteMap.put(tick, note);
	}

	public byte getVolume() {
		return volume;
	}

	public void setVolume(byte volume) {
		this.volume = volume;
	}
}

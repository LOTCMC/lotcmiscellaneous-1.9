package de.myzelyam.lotcmiscellaneous.music;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

/**
 * @author xxmicloxx, michidk, MyzelYam
 */
public class NoteSong {

	private Map<Integer, SongLayer> songLayerMap = new HashMap<Integer, SongLayer>();

	private short songHeight, length;

	private File path;

	private String author, description, title;

	private float speed, delay;

	public NoteSong(NoteSong other) {
		this.speed = other.getSpeed();
		delay = 20 / speed;
		this.songLayerMap = other.getSongLayerMap();
		this.songHeight = other.getSongHeight();
		this.length = other.getLength();
		this.title = other.getTitle();
		this.author = other.getAuthor();
		this.description = other.getDescription();
		this.path = other.getPath();
	}

	public NoteSong(float speed, Map<Integer, SongLayer> songLayerMap,
			short songHeight, final short length, String title, String author,
			String description, File path) {
		this.speed = speed;
		delay = 20 / speed;
		this.songLayerMap = songLayerMap;
		this.songHeight = songHeight;
		this.length = length;
		this.title = title;
		this.author = author;
		this.description = description;
		this.path = path;
	}

	public Map<Integer, SongLayer> getSongLayerMap() {
		return songLayerMap;
	}

	public short getSongHeight() {
		return songHeight;
	}

	public short getLength() {
		return length;
	}

	public String getTitle() {
		if (title.equalsIgnoreCase("")
				&& path.getName().equalsIgnoreCase("Winter Wrap Up.nbs")) {
			return "Winter Wrap Up";
		}
		return title;
	}

	public String getAuthor() {
		return author;
	}

	public File getPath() {
		return path;
	}

	public String getDescription() {
		return description;
	}

	public float getSpeed() {
		return speed;
	}

	public float getDelay() {
		return delay;
	}
}

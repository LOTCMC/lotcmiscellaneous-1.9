package de.myzelyam.lotcmiscellaneous.music;

import java.util.HashSet;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.annotation.CheckForNull;

import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import de.myzelyam.lotcmiscellaneous.music.events.AsyncSongEndEvent;
import de.myzelyam.lotcmiscellaneous.music.events.SongStartEvent;

public class SongPlayer {

	private final MusicModule module;

	private Map<NoteSong, SongPlayerThread> songThreadMap;

	public SongPlayer(MusicModule module) {
		this.module = module;
		songThreadMap = new ConcurrentHashMap<>();
	}

	public MusicModule getModule() {
		return module;
	}

	public void playSong(Player player, NoteSong song) {
		SongStartEvent startEvent = new SongStartEvent(player, song);
		Bukkit.getPluginManager().callEvent(startEvent);
		if (!startEvent.isCancelled()) {
			stopPlayingSong(player);
			getSongPlayerThread(song).playerSongTicks.put(player, (short) 0);
		}
	}

	public boolean hasPlayingSong(Player player) {
		return getPlayingSong(player) != null;
	}

	public void stopPlayingSong(Player player) {
		if (getPlayingSong(player) != null)
			getSongPlayerThread(getPlayingSong(player)).playerSongTicks
					.remove(player);
	}

	@CheckForNull
	public NoteSong getPlayingSong(Player player) {
		for (SongPlayerThread thread : songThreadMap.values()) {
			if (thread.playerSongTicks.keySet().contains(player))
				return thread.song;
		}
		return null;
	}

	@CheckForNull
	private SongPlayerThread startSongPlayerThread(NoteSong song) {
		SongPlayerThread spt = new SongPlayerThread(song);
		spt.runTaskAsynchronously(module.getPlugin());
		songThreadMap.put(song, spt);
		return spt;
	}

	SongPlayerThread getSongPlayerThread(NoteSong song) {
		SongPlayerThread thread = songThreadMap.get(song);
		if (thread == null)
			return startSongPlayerThread(song);
		return thread;
	}

	public void terminateEverything() {
		for (SongPlayerThread thread : songThreadMap.values()) {
			thread.setEnd(true);
		}
	}

	public class SongPlayerThread extends BukkitRunnable {

		private NoteSong song;

		private Map<Player, Short> playerSongTicks;

		private boolean end = false;

		private SongPlayerThread(NoteSong song) {
			playerSongTicks = new ConcurrentHashMap<>();
			this.song = song;
		}

		@Override
		public void run() {
			// tick loop
			while (!end) {
				long millisBefore = System.currentTimeMillis();
				// play notes
				for (Player p : new HashSet<>(playerSongTicks.keySet())) {
					short tick = playerSongTicks.get(p);
					// check for song end and call event
					if (tick > song.getLength()) {
						playerSongTicks.remove(p);
						AsyncSongEndEvent endEvent = new AsyncSongEndEvent(p,
								song);
						Bukkit.getPluginManager().callEvent(endEvent);
						continue;
					}
					playTick(p, tick, song);
					// increase tick for player
					playerSongTicks.put(p, (short) (tick + 1));
				}
				// wait for next tick
				long millisNow = System.currentTimeMillis();
				long delayBy = (long) ((song.getDelay() * 50)
						- (millisNow - millisBefore));
				if (delayBy > 0)
					try {
						Thread.sleep(delayBy);
					} catch (InterruptedException e) {
						setEnd(true);
					}
				if (!Bukkit.getPluginManager()
						.isPluginEnabled("LOTCMiscellaneous"))
					end = true;
			}
			// end actions
			if (end) {
				// empty
				playerSongTicks.clear();
				// make gc ready
				for (NoteSong song : new HashSet<>(songThreadMap.keySet())) {
					if (songThreadMap.get(song) == this)
						songThreadMap.remove(song);
				}
			}
		}

		public void setEnd(boolean end) {
			this.end = end;
		}

		public boolean isEnd() {
			return end;
		}

		private void playTick(Player player, short tick, NoteSong song) {
			for (SongLayer layer : song.getSongLayerMap().values()) {
				Note note = layer.getNote(tick);
				if (note == null) {
					continue;
				}
				float volume = (layer.getVolume()) / 100f;
				player.playSound(player.getEyeLocation(),
						NoteInstrument.getInstrument(note.getInstrument()),
						volume, NotePitch.getPitch(note.getKey() - 33));
			}
		}
	}
}

enum NotePitch {

	NOTE_0(
			0,
			0.5F),
	NOTE_1(
			1,
			0.53F),
	NOTE_2(
			2,
			0.56F),
	NOTE_3(
			3,
			0.6F),
	NOTE_4(
			4,
			0.63F),
	NOTE_5(
			5,
			0.67F),
	NOTE_6(
			6,
			0.7F),
	NOTE_7(
			7,
			0.76F),
	NOTE_8(
			8,
			0.8F),
	NOTE_9(
			9,
			0.84F),
	NOTE_10(
			10,
			0.9F),
	NOTE_11(
			11,
			0.94F),
	NOTE_12(
			12,
			1.0F),
	NOTE_13(
			13,
			1.06F),
	NOTE_14(
			14,
			1.12F),
	NOTE_15(
			15,
			1.18F),
	NOTE_16(
			16,
			1.26F),
	NOTE_17(
			17,
			1.34F),
	NOTE_18(
			18,
			1.42F),
	NOTE_19(
			19,
			1.5F),
	NOTE_20(
			20,
			1.6F),
	NOTE_21(
			21,
			1.68F),
	NOTE_22(
			22,
			1.78F),
	NOTE_23(
			23,
			1.88F),
	NOTE_24(
			24,
			2.0F);

	public int note;

	public float pitch;

	private NotePitch(int note, float pitch) {
		this.note = note;
		this.pitch = pitch;
	}

	public static float getPitch(int note) {
		for (NotePitch notePitch : values()) {
			if (notePitch.note == note) {
				return notePitch.pitch;
			}
		}
		return 0.0F;
	}
}

class NoteInstrument {

	public static Sound getInstrument(byte instrument) {
		switch (instrument) {
		case 0:
			return Sound.BLOCK_NOTE_HARP;
		case 1:
			return Sound.BLOCK_NOTE_BASS;
		case 2:
			return Sound.BLOCK_NOTE_BASEDRUM;
		case 3:
			return Sound.BLOCK_NOTE_SNARE;
		case 4:
                        return Sound.BLOCK_NOTE_HAT;
		default:
			return Sound.BLOCK_NOTE_HARP;
		}
	}

	public static org.bukkit.Instrument getBukkitInstrument(byte instrument) {
		switch (instrument) {
		case 0:
			return org.bukkit.Instrument.PIANO;
		case 1:
			return org.bukkit.Instrument.BASS_GUITAR;
		case 2:
			return org.bukkit.Instrument.BASS_DRUM;
		case 3:
			return org.bukkit.Instrument.SNARE_DRUM;
		case 4:
			return org.bukkit.Instrument.STICKS;
		default:
			return org.bukkit.Instrument.PIANO;
		}
	}
}
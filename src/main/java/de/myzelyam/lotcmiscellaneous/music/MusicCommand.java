package de.myzelyam.lotcmiscellaneous.music;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.command.*;
import org.bukkit.entity.Player;

import de.myzelyam.lotcmiscellaneous.LOTCMiscellaneous;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.*;
import net.md_5.bungee.api.chat.ComponentBuilder.FormatRetention;

public class MusicCommand implements CommandExecutor {

	private LOTCMiscellaneous plugin;

	private MusicModule module;

	public MusicCommand(LOTCMiscellaneous plugin, MusicModule module) {
		this.plugin = plugin;
		this.module = module;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command,
			String label, String[] args) {
		if (!(sender instanceof Player))
			return false;
		Player p = (Player) sender;
		if (args.length < 1) {
			return false;
		}
		String action = args[0];
		if (action.equalsIgnoreCase("toggle") || action.equalsIgnoreCase("off")
				|| action.equalsIgnoreCase("start")
				|| action.equalsIgnoreCase("stop")
				|| action.equalsIgnoreCase("on")
				|| action.equalsIgnoreCase("enable")
				|| action.equalsIgnoreCase("disable")) {
			boolean enabled = plugin.data.getBoolean("Music.PlayerData."
					+ p.getUniqueId().toString() + ".musicEnabled", true);
			plugin.data.set("Music.PlayerData." + p.getUniqueId().toString()
					+ ".musicEnabled", !enabled);
			plugin.dataFile.save();
			p.sendMessage("§6Your music is now "
					+ (enabled ? "§cdisabled" : "§aenabled") + "§6!");
			if (!enabled) {
				// enable
				module.getSongPlayer().playSong(p, module.getRandomNoteSong());
				module.sendSongMsg(p);
			} else {
				// disable
				module.getSongPlayer().stopPlayingSong(p);
			}
			return true;
		}
		if (action.equalsIgnoreCase("next")
				|| action.equalsIgnoreCase("random")) {
			boolean enabled = plugin.data.getBoolean("Music.PlayerData."
					+ p.getUniqueId().toString() + ".musicEnabled", true);
			if (!enabled) {
				plugin.data.set("Music.PlayerData." + p.getUniqueId().toString()
						+ ".musicEnabled", !enabled);
				plugin.dataFile.save();
				p.sendMessage("§6Your music is now "
						+ (enabled ? "§cdisabled" : "§aenabled") + "§6!");
			}
			NoteSong current = module.getSongPlayer().getPlayingSong(p);
			NoteSong song = (module.getMusicWorldNames()
					.contains(p.getWorld().getName())
							? module.getRandomNoteSong(p.getWorld().getName())
							: module.getRandomNoteSong());
			for (int i = 0; i < 20; i++) {
				if (song == current)
					song = (module.getMusicWorldNames()
							.contains(p.getWorld().getName()) ? module
									.getRandomNoteSong(p.getWorld().getName())
									: module.getRandomNoteSong());
			}
			module.getSongPlayer().playSong(p, song);
			module.sendSongMsg(p);
			return true;
		}
		if (action.equalsIgnoreCase("current")
				|| action.equalsIgnoreCase("currentsong")
				|| action.equalsIgnoreCase("playing")
				|| action.equalsIgnoreCase("now")) {
			NoteSong song = module.getSongPlayer().getPlayingSong(p);
			if (song == null) {
				p.sendMessage("§cNo song is playing currently!");
				return true;
			}
			p.sendMessage("§6Current song: §e" + song.getTitle() + "§6 by §e"
					+ song.getAuthor());
			return true;
		}
		if (action.equalsIgnoreCase("list") || action.equalsIgnoreCase("songs")
				|| action.equalsIgnoreCase("songlist")) {
			ComponentBuilder songs = new ComponentBuilder("Available songs: ");
			songs = songs.color(ChatColor.RED);
			HoverEvent he = new HoverEvent(HoverEvent.Action.SHOW_TEXT,
					new ComponentBuilder("Click to play!").color(ChatColor.AQUA)
							.create());
			boolean isLast = false;
			List<String> titles = new ArrayList<String>(
					module.getTitleSongMap().keySet());
			for (int i = 0; i < titles.size(); i++) {
				isLast = (i == (titles.size() - 1));
				String title = titles.get(i);
				ClickEvent ce = new ClickEvent(ClickEvent.Action.RUN_COMMAND,
						"/music " + title);
				songs = songs.append(title).color(ChatColor.YELLOW).event(ce)
						.event(he);
				if (!isLast)
					songs = songs.append(", ", FormatRetention.NONE)
							.color(ChatColor.GOLD);
			}
			p.spigot().sendMessage(songs.create());
			return true;
		}
		// try to find song
		String argsStr = "";
		for (String arg : args) {
			argsStr = argsStr.concat(arg + " ");
		}
		if (argsStr.length() > 1)
			argsStr = argsStr.substring(0, argsStr.length() - 1);
		NoteSong song = module.getSongByString(argsStr);
		if (song != null) {
			boolean enabled = plugin.data.getBoolean("Music.PlayerData."
					+ p.getUniqueId().toString() + ".musicEnabled", true);
			if (!enabled) {
				plugin.data.set("Music.PlayerData." + p.getUniqueId().toString()
						+ ".musicEnabled", !enabled);
				plugin.dataFile.save();
				p.sendMessage("§6Your music is now "
						+ (enabled ? "§cdisabled" : "§aenabled") + "§6!");
			}
			module.getSongPlayer().playSong(p, song);
			module.sendSongMsg(p);
			return true;
		}
		p.sendMessage("§cInvalid usage or invalid song. => /music");
		return true;
	}
}

package de.myzelyam.lotcmiscellaneous.music.events;

import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerEvent;

import de.myzelyam.lotcmiscellaneous.music.NoteSong;

public class AsyncSongEndEvent extends PlayerEvent {

	private static final HandlerList handlers = new HandlerList();

	private NoteSong song;

	public AsyncSongEndEvent(Player who, NoteSong song) {
		super(who);
		this.song = song;
	}

	public NoteSong getEndedSong() {
		return song;
	}

	@Override
	public HandlerList getHandlers() {
		return handlers;
	}

	public static HandlerList getHandlerList() {
		return handlers;
	}
}

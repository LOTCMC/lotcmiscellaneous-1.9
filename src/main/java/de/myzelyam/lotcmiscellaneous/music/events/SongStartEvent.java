package de.myzelyam.lotcmiscellaneous.music.events;

import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerEvent;

import de.myzelyam.lotcmiscellaneous.music.NoteSong;

/**
 * Danger: May be called async
 */
public class SongStartEvent extends PlayerEvent implements Cancellable {

	private static final HandlerList handlers = new HandlerList();

	private NoteSong song;

	private boolean cancelled = false;

	public SongStartEvent(Player who, NoteSong song) {
		super(who);
		this.song = song;
	}

	public NoteSong getStartingSong() {
		return song;
	}

	@Override
	public HandlerList getHandlers() {
		return handlers;
	}

	public static HandlerList getHandlerList() {
		return handlers;
	}

	@Override
	public boolean isCancelled() {
		return cancelled;
	}

	@Override
	public void setCancelled(boolean cancel) {
		cancelled = cancel;
	}
}

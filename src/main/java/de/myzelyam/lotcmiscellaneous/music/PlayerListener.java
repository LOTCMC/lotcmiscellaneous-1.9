package de.myzelyam.lotcmiscellaneous.music;

import java.util.List;

import org.bukkit.*;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.*;
import org.bukkit.scheduler.BukkitRunnable;

import de.myzelyam.lotcmiscellaneous.music.events.AsyncSongEndEvent;
import de.myzelyam.lotcmiscellaneous.utils.StringUtils;

public class PlayerListener implements Listener {

	private MusicModule module;

	private List<String> musicWorlds;

	public PlayerListener(MusicModule module) {
		this.module = module;
		Bukkit.getPluginManager().registerEvents(this, module.getPlugin());
		musicWorlds = module.getMusicWorldNames();
	}

	@EventHandler
	public void onJoin(final PlayerJoinEvent e) {
		if (module.getAllNoteSongs().isEmpty())
			return;
		Location loc = e.getPlayer().getLocation();
		if (isEnabled(e.getPlayer()))
			if (StringUtils.containsIgnoreCase(musicWorlds,
					loc.getWorld().getName())) {
				// play music
				NoteSong song = module
						.getRandomNoteSong(e.getPlayer().getWorld().getName());
				if (song == null)
					return;
				module.getSongPlayer().playSong(e.getPlayer(), song);
				module.sendSongMsg(e.getPlayer());
				new BukkitRunnable() {

					@Override
					public void run() {
						e.getPlayer().sendMessage(ChatColor.AQUA + "> "
								+ ChatColor.GOLD + "You can use "
								+ ChatColor.YELLOW + "/music toggle"
								+ ChatColor.GOLD + " to toggle your music!");
					}
				}.runTaskLater(module.getPlugin(), 10);
			}
	}

	@EventHandler
	public void onWC(PlayerChangedWorldEvent e) {
		if (module.getAllNoteSongs().isEmpty())
			return;
		String to = e.getPlayer().getWorld().getName();
		if (StringUtils.containsIgnoreCase(musicWorlds, to)) {
			// play music
			if (isEnabled(e.getPlayer())) {
				NoteSong song = module
						.getRandomNoteSong(e.getPlayer().getWorld().getName());
				if (song == null)
					return;
				module.getSongPlayer().playSong(e.getPlayer(), song);
				module.sendSongMsg(e.getPlayer());
			}
		} else {
			// stop playing
			module.getSongPlayer().stopPlayingSong(e.getPlayer());
		}
	}

	@EventHandler
	public void onQuit(PlayerQuitEvent e) {
		// remove from map to prevent memory leaks
		module.getSongPlayer().stopPlayingSong(e.getPlayer());
	}

	@EventHandler
	public void onSongEnd(final AsyncSongEndEvent e) {
		final Player p = e.getPlayer();
		Location loc = e.getPlayer().getLocation();
		if (StringUtils.containsIgnoreCase(musicWorlds,
				loc.getWorld().getName()) && isEnabled(p)) {
			if (module.getPlugin().getConfig().getBoolean("RepeatMusic")) {
				new BukkitRunnable() {

					@Override
					public void run() {
						// play music
						NoteSong song = module.getRandomNoteSong(
								e.getPlayer().getWorld().getName());
						for (int i = 0; i < 20; i++) {
							if (song == e.getEndedSong())
								song = module.getRandomNoteSong(
										e.getPlayer().getWorld().getName());
						}
						if (song == null)
							return;
						module.getSongPlayer().playSong(e.getPlayer(), song);
						module.sendSongMsg(p);
					}
				}.runTaskLaterAsynchronously(module.getPlugin(), 40);
			}
		}
	}

	private boolean isEnabled(Player p) {
		boolean enabled = module.getPlugin().data.getBoolean("Music.PlayerData."
				+ p.getUniqueId().toString() + ".musicEnabled", true);
		return enabled;
	}
}

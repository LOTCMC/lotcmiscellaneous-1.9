package de.myzelyam.lotcmiscellaneous.bungee;

import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class ServerCmd extends Command {

	ServerInfo server;

	public ServerCmd(ServerInfo si, String name) {
		super(name);
		this.server = si;
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		if (sender instanceof ProxiedPlayer) {
			ProxiedPlayer p = (ProxiedPlayer) sender;
			p.connect(server);
		}
	}
}

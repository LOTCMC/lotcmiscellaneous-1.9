package de.myzelyam.lotcmiscellaneous.bungee;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.ServerKickEvent;
import net.md_5.bungee.api.event.ServerSwitchEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.api.scheduler.ScheduledTask;
import net.md_5.bungee.event.EventHandler;
import net.md_5.bungee.event.EventPriority;

public class MoveToLimbo implements Listener {

	public static final List<String> blacklistedWordsForRedirecting = Arrays
			.asList("kicked", "banned", "AAC");

	private final LOTCBungee plugin;

	private String limbo, lobby;

	private List<ProxiedPlayer> limboTPPlayers = new LinkedList<>();

	public MoveToLimbo(LOTCBungee plugin) {
		plugin.getProxy().getPluginManager().registerListener(plugin, this);
		this.plugin = plugin;
		limbo = plugin.getConfig().getString("LimboServer");
		lobby = plugin.getConfig().getString("MainServer");
	}

	@SuppressWarnings("deprecation")
	@EventHandler(priority = EventPriority.HIGH)
	public void onDC(ServerKickEvent e) {
		ServerInfo from = e.getKickedFrom();
		ServerInfo target = plugin.getProxy().getServerInfo(limbo);
		if (from == null || from.getName().equals(limbo)) {
			return;
		}
		String reason = e.getKickReason();
		// only do it if the reason has been validated
		if (!validateKickReason(reason))
			return;
		e.setCancelled(true);
		e.setCancelServer(target);
		startMoveBackTimer(e.getPlayer(), from, reason);
	}

	@EventHandler(priority = EventPriority.HIGH)
	public void onDC(ServerSwitchEvent e) {
		if (e.getPlayer().getServer() == null)
			return;
		if (!e.getPlayer().getServer().getInfo().getName().equals("Limbo"))
			return;
		ServerInfo target = plugin.getProxy().getServerInfo(lobby);
		startMoveBackTimer(e.getPlayer(), target, null);
	}

	public boolean validateKickReason(String reason) {
		if (reason == null)
			return true;
		for (String word : blacklistedWordsForRedirecting) {
			if (reason.toLowerCase().contains(word.toLowerCase()))
				return false;
		}
		return true;
	}

	public void startMoveBackTimer(ProxiedPlayer p, final ServerInfo moveTo,
			final String kickReason) {
		boolean notAddedBefore = limboTPPlayers.add(p);
		// don't repeat
		if (!notAddedBefore)
			return;
		final UUID uuid = p.getUniqueId();
		// keep connecting until it succeeds or the player quits
		final AtomicReference<ScheduledTask> taskRef = new AtomicReference<ScheduledTask>();
		final AtomicInteger taskTimer = new AtomicInteger(1);
		taskRef.set(plugin.getProxy().getScheduler().schedule(plugin,
				new Runnable() {

					@SuppressWarnings("deprecation")
					@Override
					public void run() {
						int timer = taskTimer.getAndIncrement();
                                                ProxiedPlayer p = ProxyServer.getInstance().getPlayer(uuid);
                                                
//                                              Myzel why you no use updated references
//						ProxiedPlayer p = BungeeCord.getInstance().getPlayer(uuid);
                                                
						// cancel if player is not on bungee anymore or not in limbo
						if (p == null || !p.getServer().getInfo().getName()
								.equals(limbo)) {
							ScheduledTask task = taskRef.get();
							if (task != null) {
								limboTPPlayers.remove(p);
								task.cancel();
							}
							return;
						}
						// send info msg
						if (timer == 2) {
							if (kickReason != null)
								p.sendMessages(new String[] {
										ChatColor.RED
												+ "You have been kicked from your previous server:",
										kickReason,
										ChatColor.YELLOW + "" + ChatColor.BOLD
												+ "Please wait! "
												+ ChatColor.AQUA + ""
												+ ChatColor.BOLD
												+ "You will be connected back to where you came from "
												+ ChatColor.UNDERLINE
												+ "shortly" + ChatColor.RESET
												+ ChatColor.AQUA
												+ ChatColor.BOLD + "." });
							// attempt to reconnect every 40 seconds up to 6
							// times (max time = 40s * 6 = 240s = 4m)
						} else if (timer == 40 || timer == 80 || timer == 120
								|| timer == 160 || timer == 200
								|| timer == 240) {
							p.connect(moveTo);
							// adjust action bar
						} else if (timer < 120) {
							int current = 40;
							if (timer > 40)
								current = 80;
							if (timer > 80)
								current = 120;
							if (timer > 120)
								current = 160;
							if (timer > 160)
								current = 200;
							if (timer > 200)
								current = 240;
							p.sendMessage(ChatMessageType.ACTION_BAR,
									new ComponentBuilder("Reconnecting in "
											+ (current - timer) + " seconds")
													.color(ChatColor.GOLD)
													.create());
							// end
						} else if (timer > 240) {
							p.sendMessage(ChatColor.RED
									+ "We were unable to connect you back to where you came from, please relog manually.");
							ScheduledTask task = taskRef.get();
							if (task != null) {
								limboTPPlayers.remove(p);
								task.cancel();
							}
						}
					}
				}, 1, 1, TimeUnit.SECONDS));
	}
}

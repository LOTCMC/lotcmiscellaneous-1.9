package de.myzelyam.lotcmiscellaneous.bungee;

import java.util.concurrent.TimeUnit;

import de.myzelyam.lotcmiscellaneous.files.FileMgr;
import de.myzelyam.lotcmiscellaneous.files.FileMgr.FileType;
import de.myzelyam.lotcmiscellaneous.files.PluginFile;

import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.api.plugin.PluginManager;
import net.md_5.bungee.config.Configuration;

public class LOTCBungee extends Plugin {

	private static LOTCBungee instance;

	public static LOTCBungee getInstance() {
		return instance;
	}

	private FileMgr fileMgr;

	private PluginFile<Configuration> configFile;

	private Configuration config;

	@SuppressWarnings("unchecked")
	@Override
	public void onEnable() {
		instance = this;
		fileMgr = new FileMgr();
		configFile = (PluginFile<Configuration>) fileMgr
				.addFile("configuration", FileType.BUNGEE_CONFIG);
		setConfig(configFile.getConfig());
		// setup own bungee command modifications when all plugins and modules
		// are loaded
		final Plugin plugin = this;
		getProxy().getScheduler().schedule(this, new Runnable() {

			@Override
			public void run() {
				PluginManager pm = getProxy().getPluginManager();
				if (!(getProxy().getDisabledCommands().contains("send")
						|| pm.getPlugin("cmd_send") == null)) {
					pm.unregisterCommands(pm.getPlugin("cmd_send"));
					pm.registerCommand(plugin, new CustomSendCmd());
				}
			}
		}, 10, TimeUnit.SECONDS);
		// commands
		PluginManager pm = getProxy().getPluginManager();
		for (ServerInfo si : getProxy().getServers().values()) {
			pm.registerCommand(this, new ServerCmd(si, si.getName()));
		}
		pm.registerCommand(this, new PingCmd(this));
		pm.registerCommand(this, new GStopCmd(this));
		// register dc listener
		new MoveToLimbo(this);
	}

	@Override
	public void onDisable() {
		instance = null;
	}

	public Configuration getConfig() {
		return config;
	}

	private void setConfig(Configuration config) {
		this.config = config;
	}
}

package de.myzelyam.lotcmiscellaneous.bungee;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class PingCmd extends Command {

	private LOTCBungee plugin;

	public PingCmd(LOTCBungee plugin) {
		super("ping", "lotc.ping");
		this.plugin = plugin;
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		boolean isPlayer = sender instanceof ProxiedPlayer;
		boolean canOther = sender.hasPermission("lotc.ping.other");
		ProxiedPlayer target = isPlayer ? (ProxiedPlayer) sender : null;
		// self
		if (args.length < 1) {
			if (target == null) {
				usage(sender);
				return;
			}
			ping(sender, target);
			return;
		}
		// other
		if (!canOther) {
			noPerm(sender);
			return;
		}
		target = plugin.getProxy().getPlayer(args[0]);
		if (target == null) {
			notFound(sender, args[0]);
			return;
		}
		ping(sender, target);
	}

	@SuppressWarnings("deprecation")
	private void notFound(CommandSender sender, String string) {
		sender.sendMessage(ChatColor.AQUA + "LOTC > " + ChatColor.RED
				+ "Player not found");
	}

	@SuppressWarnings("deprecation")
	private void ping(CommandSender sender, ProxiedPlayer target) {
		sender.sendMessage(ChatColor.AQUA + target.getDisplayName()
				+ "'s ping is " + ChatColor.GOLD + "" + target.getPing());
	}

	@SuppressWarnings("deprecation")
	private void usage(CommandSender sender) {
		sender.sendMessage(ChatColor.AQUA + "Usage > " + ChatColor.GOLD
				+ "/ping <player>");
	}

	@SuppressWarnings("deprecation")
	private void noPerm(CommandSender sender) {
		sender.sendMessage(
				ChatColor.AQUA + "LOTC > " + ChatColor.RED + "No permission");
	}
}

package de.myzelyam.lotcmiscellaneous.bungee;

import java.util.concurrent.TimeUnit;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class GStopCmd extends Command {

	private LOTCBungee plugin;

	public GStopCmd(LOTCBungee plugin) {
		super("gstop", "lotc.gstop");
		this.plugin = plugin;
	}

	@Override
	public void execute(CommandSender sender, String[] args) {
		String reasonArg = "We're restarting our proxy, we'll most likely be back in less than 30 seconds.";
		if (args.length > 0) {
			for (int i = 0; i < args.length; i++) {
				reasonArg += args[i] + " ";
			}
			reasonArg = reasonArg.substring(0, reasonArg.length() - 1);
		}
		ComponentBuilder reason = new ComponentBuilder("[");
		reason = reason.color(ChatColor.AQUA).append("Proxy Restarting")
				.color(ChatColor.RED).append("]").color(ChatColor.AQUA)
				.append("\n" + reasonArg).color(ChatColor.YELLOW)
				.append("\nWe apologize for any inconvenience caused")
				.color(ChatColor.GREEN);
		final ComponentBuilder finalReason = reason;
		kickAll(finalReason.create());
		plugin.getProxy().getScheduler().schedule(plugin, new Runnable() {

			@Override
			public void run() {
				kickAll(finalReason.create());
				plugin.getProxy().stop();
			}
		}, 2, TimeUnit.SECONDS);
	}

	public void kickAll(BaseComponent[] reason) {
		for (ProxiedPlayer p : plugin.getProxy().getPlayers()) {
			p.disconnect(reason);
		}
	}
}

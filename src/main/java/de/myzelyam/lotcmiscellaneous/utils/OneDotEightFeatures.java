package de.myzelyam.lotcmiscellaneous.utils;

import java.lang.reflect.InvocationTargetException;

import javax.annotation.Nullable;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.wrappers.EnumWrappers.TitleAction;
import com.comphenix.protocol.wrappers.WrappedChatComponent;

public abstract class OneDotEightFeatures {

	public static void sendActionBar(Player p, String text) {
		String json = "{\"text\": \""
				+ ChatColor.translateAlternateColorCodes('&', text) + "\"}";
		WrappedChatComponent msg = WrappedChatComponent.fromJson(json);
		PacketContainer chatMsg = new PacketContainer(
				PacketType.Play.Server.CHAT);
		chatMsg.getChatComponents().write(0, msg);
		chatMsg.getBytes().write(0, (byte) 2);
		try {
			ProtocolLibrary.getProtocolManager().sendServerPacket(p, chatMsg);
		} catch (InvocationTargetException e) {
			throw new RuntimeException("Cannot send packet " + chatMsg, e);
		}
	}

	public static void sendTitle(Player p, String title,
			@Nullable String subTitle, int fadeIn, int stay, int fadeOut) {
		String json = "{\"text\": \""
				+ ChatColor.translateAlternateColorCodes('&', title) + "\"}";
		WrappedChatComponent chatComponent = WrappedChatComponent
				.fromJson(json);
		PacketContainer titlePacket = new PacketContainer(
				PacketType.Play.Server.TITLE);
		titlePacket.getChatComponents().write(0, chatComponent);
		titlePacket.getTitleActions().write(0, TitleAction.TITLE);
		titlePacket.getIntegers().write(0, fadeIn);
		titlePacket.getIntegers().write(1, stay);
		titlePacket.getIntegers().write(2, fadeOut);
		try {
			ProtocolLibrary.getProtocolManager().sendServerPacket(p,
					titlePacket);
		} catch (InvocationTargetException e) {
			throw new RuntimeException("Cannot send packet " + titlePacket, e);
		}
		// subTitle
		if (subTitle == null || subTitle.equalsIgnoreCase(""))
			return;
		json = "{\"text\": \""
				+ ChatColor.translateAlternateColorCodes('&', subTitle) + "\"}";
		chatComponent = WrappedChatComponent.fromJson(json);
		titlePacket = new PacketContainer(PacketType.Play.Server.TITLE);
		titlePacket.getChatComponents().write(0, chatComponent);
		titlePacket.getTitleActions().write(0, TitleAction.SUBTITLE);
		titlePacket.getIntegers().write(0, fadeIn);
		titlePacket.getIntegers().write(1, stay);
		titlePacket.getIntegers().write(2, fadeOut);
		try {
			ProtocolLibrary.getProtocolManager().sendServerPacket(p,
					titlePacket);
		} catch (InvocationTargetException e) {
			throw new RuntimeException("Cannot send packet " + titlePacket, e);
		}
	}
}

package de.myzelyam.lotcmiscellaneous.utils;

public abstract class MathUtils {

	public static float roundToTwoDecimals(float number) {
		return (Math.round(number * 100f) / 100f);
	}
}

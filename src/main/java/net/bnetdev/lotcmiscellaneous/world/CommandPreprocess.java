/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.bnetdev.lotcmiscellaneous.world;

import net.md_5.bungee.api.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

public class CommandPreprocess implements Listener {

    private final FlyModule module;

    public CommandPreprocess(FlyModule module) {
        this.module = module;
    }

    @EventHandler
    public void onCommandPreprocess(PlayerCommandPreprocessEvent e) {
        if (this.module.getPlugin().getConfig().getBoolean("FlyModule.worlds.".concat(e.getPlayer().getWorld().getName()).concat(".enabled"), false)) {
            String replace = e.getMessage().replace("/", "");
            if (replace.equalsIgnoreCase("fly") && e.getPlayer().hasPermission(this.module.getPlugin().getConfig().getString("FlyModule.worlds.".concat(e.getPlayer().getWorld().getName()).concat(".permission")))) {
                if (!e.getPlayer().isFlying()) {
                    e.getPlayer().sendMessage(ChatColor.translateAlternateColorCodes('&', this.module.getPlugin().getConfig().getString("FlyModule.worlds.".concat(e.getPlayer().getWorld().getName()).concat(".prohibittedmessage"))));
                    e.setCancelled(true);
                }
            }
        }
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.bnetdev.lotcmiscellaneous.world;

import de.myzelyam.lotcmiscellaneous.LOTCMiscellaneous;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

public class PlayerScanner extends Object implements Runnable {

    private LOTCMiscellaneous plugin;
    private String world;

    public PlayerScanner(LOTCMiscellaneous instance, String w) {
        this.plugin = instance;
        this.world = w;
    }

    @Override
    public void run() {
        for (Player p : Bukkit.getOnlinePlayers()) {
            if (p.getWorld().getName().equalsIgnoreCase(this.world)) {
                if (p.isFlying()) {
                    if (p.hasPermission(this.plugin.getConfig().getString("FlyModule.worlds.".concat(this.world).concat(".permission")))) {
                        p.sendMessage(ChatColor.translateAlternateColorCodes('&', this.plugin.getConfig().getString("FlyModule.worlds.".concat(this.world).concat(".prohibittedmessage"))));

                        int height = 0;
                        int maxHeight = p.getWorld().getMaxHeight();
                        Block block;

                        while (height <= maxHeight) {
                            height++;
                            block = p.getLocation().add(0, height, 0).getBlock();
                            if (block.isEmpty()) {
                                block = p.getEyeLocation().add(0, height, 0).getBlock();
                                if (block.isEmpty()) {
                                    p.teleport(p.getLocation().add(0, height, 0));
                                }
                            }
                        }
                        
                        p.setFlying(false);
                    }
                }
            }
        }
    }

}

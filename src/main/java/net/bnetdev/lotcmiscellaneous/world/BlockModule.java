package net.bnetdev.lotcmiscellaneous.world;

import de.myzelyam.lotcmiscellaneous.AbstractModule;
import de.myzelyam.lotcmiscellaneous.LOTCMiscellaneous;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockDamageEvent;
import org.bukkit.event.block.BlockPlaceEvent;

import java.util.ArrayList;
import java.util.List;

public class BlockModule extends AbstractModule implements Listener {

    private List<Material> blocks = new ArrayList<>();

    public BlockModule(LOTCMiscellaneous instance) {
        super(instance);
    }

    @Override
    public boolean enable() {
        Bukkit.getPluginManager().registerEvents(this, this.getPlugin());

        for(String s : this.getPlugin().getConfig().getStringList("BlockControl.blocks")) {
            Material m = Material.getMaterial(s);
            blocks.add(m);
        }
        return true;
    }

    @Override
    public void disable(LOTCMiscellaneous plugin) {
    }

    @EventHandler
    public void onBlockPlace(BlockPlaceEvent event) {
        if(blocks.contains(event.getBlock().getType()) && !event.getPlayer().hasPermission("lotc.blockcontrol." + event.getBlock().getType().name())) {
            event.getPlayer().sendMessage(ChatColor.translateAlternateColorCodes('&', this.getPlugin().getConfig().getString("BlockControl.message").replace("%b", event.getBlock().getType().name())));
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onBlockDamage(BlockDamageEvent event) {
        if(blocks.contains(event.getBlock().getType()) && !event.getPlayer().hasPermission("lotc.blockcontrol." + event.getBlock().getType().name())) {
            event.getPlayer().sendMessage(ChatColor.translateAlternateColorCodes('&', this.getPlugin().getConfig().getString("BlockControl.message").replace("%b", event.getBlock().getType().name())));
            event.setCancelled(true);
        }
    }

    @EventHandler
    public void onBlockBreak(BlockBreakEvent event) {
        if(blocks.contains(event.getBlock().getType()) && !event.getPlayer().hasPermission("lotc.blockcontrol." + event.getBlock().getType().name())) {
            event.getPlayer().sendMessage(ChatColor.translateAlternateColorCodes('&', this.getPlugin().getConfig().getString("BlockControl.message").replace("%b", event.getBlock().getType().name())));
            event.setCancelled(true);
        }
    }

}

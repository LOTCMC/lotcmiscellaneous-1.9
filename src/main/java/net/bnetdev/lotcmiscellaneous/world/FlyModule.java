/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.bnetdev.lotcmiscellaneous.world;

import de.myzelyam.lotcmiscellaneous.AbstractModule;
import de.myzelyam.lotcmiscellaneous.LOTCMiscellaneous;
import java.util.List;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.scheduler.BukkitScheduler;

public class FlyModule extends AbstractModule {

    public FlyModule(LOTCMiscellaneous instance) {
        super(instance);
    }

    @Override
    public boolean enable() {
        Bukkit.getPluginManager().registerEvents(new CommandPreprocess(this), this.getPlugin());

        List<World> worlds = Bukkit.getWorlds();
        BukkitScheduler scheduler = Bukkit.getServer().getScheduler();
        for (World w : worlds) {
            if (this.getPlugin().getConfig().getBoolean("FlyModule.worlds.".concat(w.getName()).concat(".enabled"), false)) {
                scheduler.scheduleSyncRepeatingTask(this.getPlugin(), new PlayerScanner(this.getPlugin(), w.getName()), 0L, 20L);
            }
        }

        return true;
    }

    @Override
    public void disable(LOTCMiscellaneous plugin) {
        BukkitScheduler scheduler = Bukkit.getServer().getScheduler();
        scheduler.cancelTasks(this.getPlugin());
    }

}
